package org.example.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public final class ConnectionManager {
    private static final Logger logConnect = LoggerFactory.getLogger(ConnectionManager.class);
    private static final String URL_KEY = "db.url";
    private static final String USERNAME_KEY = "db.username";
    private static final String PASSWORD_KEY = "db.password";

    static {
        loadDriver();
    }

    private ConnectionManager() {
    }

    private static void loadDriver() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            logConnect.debug("Driver was loaded");
        } catch (ClassNotFoundException e) {
            logConnect.error("Driver was not loaded");
            throw new RuntimeException(e);
        }
    }

    public static Connection open() {
        try {
            Connection connection = DriverManager.getConnection(
                    PropertiesUtil.get(URL_KEY),
                    PropertiesUtil.get(USERNAME_KEY),
                    PropertiesUtil.get(PASSWORD_KEY)
            );
            connection.setAutoCommit(false);
            logConnect.debug("Connection to DataBase was successful");
            return connection;
        } catch (SQLException ex) {
            logConnect.error("Connection to DataBase was fail\n" + ex);
            throw new RuntimeException(ex);
        }
    }
}
