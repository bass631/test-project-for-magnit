package org.example.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public final class PropertiesUtil {

    private static final Logger logProp = LoggerFactory.getLogger(PropertiesUtil.class);
    private static final Properties PROPERTIES = new Properties();

    static {
        loadProperties();
    }

    private PropertiesUtil() {
    }

    public static String get(String key) {
        return PROPERTIES.getProperty(key);
    }

    private static void loadProperties() {
        InputStream inputStream = PropertiesUtil.class.getClassLoader()
                .getResourceAsStream("application.properties");
        try {
            PROPERTIES.load(inputStream);
            logProp.debug("Properties was load success");
        } catch (IOException e) {
            logProp.error("Properties was not load");
            throw new RuntimeException(e);
        }
    }
}
