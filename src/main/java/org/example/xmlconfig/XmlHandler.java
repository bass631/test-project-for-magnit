package org.example.xmlconfig;

import org.example.model.Entry;
import org.w3c.dom.Document;

import java.util.List;

public interface XmlHandler {
    void writer(List<Integer> fields, String path);
    void writeDocument(Document document, String path);
    List<Entry> reader(String path);
    void transform(String pathIn, String pathOut);

}
