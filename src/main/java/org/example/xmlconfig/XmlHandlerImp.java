package org.example.xmlconfig;

import org.example.model.Entry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class XmlHandlerImp implements XmlHandler {

    private static final Logger logXml = LoggerFactory.getLogger(XmlHandlerImp.class);
    private static DocumentBuilderFactory factory;
    private static DocumentBuilder documentBuilder;

    public void writer(List<Integer> fields, String path) {

        factory = DocumentBuilderFactory.newInstance();
        Document doc = null;

        try {
            documentBuilder = factory.newDocumentBuilder();
            doc = documentBuilder.newDocument();

            Element rootElement = doc.createElement("entries");
            doc.appendChild(rootElement);

            for (Integer field : fields) {
                rootElement.appendChild(getEntry(doc, field.toString()));
            }
            logXml.debug("Document was created");
        } catch (ParserConfigurationException e) {
            logXml.error("Document was not created\n" + e);
            e.printStackTrace();
        } finally {
            if (doc != null) {
                writeDocument(doc, String.valueOf(path));
            }
        }
    }

    private static Node getEntry(Document doc, String field) {
        Element entry = doc.createElement("entry");
        entry.appendChild(getEntryElements(doc, "field", field));
        return entry;
    }

    private static Node getEntryElements(Document doc, String field, String value) {
        Element node = doc.createElement(field);
        node.appendChild(doc.createTextNode(value));
        return node;
    }

    public void writeDocument(Document document, String path) {
        try {
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(document);

            StreamResult result = new StreamResult(new File(path));
            transformer.transform(source, result);
            logXml.debug("File was created");
        } catch (TransformerException e) {
            logXml.error("File was not created\n" + e);
            e.printStackTrace();
        }
    }

    public List<Entry> reader(String path) {

        factory = DocumentBuilderFactory.newInstance();
        List<Entry> entryList = new ArrayList<>();
        try {
            documentBuilder = factory.newDocumentBuilder();
            Document document = documentBuilder.parse(path);
            document.getDocumentElement().normalize();
            Node node = document.getFirstChild();
            NodeList rootChild = node.getChildNodes();
            for (int j = 0; j < rootChild.getLength(); j++) {
                entryList.add(getEntry(rootChild.item(j)));
            }

            logXml.debug("File was read");
        } catch (ParserConfigurationException | SAXException | IOException exc) {
            logXml.error("File was not read\n" + exc);
            exc.printStackTrace();
        }
        return entryList;
    }

    private static Entry getEntry(Node node) {
        Entry entry = new Entry();
        if (node.getNodeType() == Node.ELEMENT_NODE) {
            entry.setField(Integer.parseInt(node.getTextContent()));
        }
        return entry;
    }

    public void transform(String pathIn, String pathOut) {
        StreamSource xslTsrc = new StreamSource("src/main/resources/transform.xslt");
        StreamSource pathXmlIn = new StreamSource(pathIn);
        StreamResult pathXmlOut = new StreamResult(pathOut);
        try {
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer(xslTsrc);
            transformer.transform(pathXmlIn, pathXmlOut);
            logXml.debug("File was transform");
        } catch (TransformerException e) {
            logXml.error("File was not transform\n" + e);
            e.printStackTrace();
        }

    }
}