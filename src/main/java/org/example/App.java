package org.example;

import org.example.xmlconfig.XmlHandler;
import org.example.xmlconfig.XmlHandlerImp;
import org.example.model.Entry;
import org.example.service.EntryService;
import org.example.service.EntryServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.List;

public class App {
    private static final File FILE_pathIn = new File(
            System.getProperty("user.home") + File.separator + "Desktop" + File.separator + "1.xml");
    private static final File FILE_pathOut = new File(
            System.getProperty("user.home") + File.separator + "Desktop" + File.separator + "2.xml");
    private static final int N = 5;
    private static final Logger logApp = LoggerFactory.getLogger(App.class);
    private static final EntryService ENTRY_SERVICE = new EntryServiceImpl();
    private static final XmlHandler xmlHandler = new XmlHandlerImp();

    private static void createOrUpdateTable() {
        if (ENTRY_SERVICE.tableExists()) {
            ENTRY_SERVICE.truncateTable();
        }
        ENTRY_SERVICE.createTable();
    }

    private static void createNFieldsToDb() {
        for (int i = 1; i < N + 1; i++) {
            ENTRY_SERVICE.saveField(i);
        }
    }

    private static void writeFieldsToXml() {
        List<Integer> fields = ENTRY_SERVICE.getField();
        xmlHandler.writer(fields, FILE_pathIn.getPath());
    }

    private static void transformXmlFile() {
        xmlHandler.transform(
                FILE_pathIn.getPath(),
                FILE_pathOut.getPath());
    }

    private static void printReadFromXmlFields() {
        List<Entry> entryList = xmlHandler.reader(FILE_pathIn.getPath());
        logApp.info("Печать в консоль значений всех атрибутов field");
        int sumResult = entryList.stream()
                .peek(System.out::println)
                .mapToInt(Entry::getField)
                .sum();
        logApp.info("Арифметическая сумму значений всех атрибутов field = " + (sumResult));
    }

    private static void run() {
        createOrUpdateTable();
        createNFieldsToDb();
        writeFieldsToXml();
        transformXmlFile();
        printReadFromXmlFields();
    }

    public static void main(String[] args) {
        run();
    }
}
