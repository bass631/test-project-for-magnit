package org.example.model;

public class Entry {
    private int field;

    public int getField() {
        return field;
    }

    public void setField(int field) {
        this.field = field;
    }

    public Entry() {
    }

    @Override
    public String toString() {
        return "Entry {" +
                "field=" + field +
                '}';
    }
}
