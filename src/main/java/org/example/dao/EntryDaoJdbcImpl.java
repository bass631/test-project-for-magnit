
package org.example.dao;

import org.example.util.ConnectionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class EntryDaoJdbcImpl implements EntryDao {

    private static final Logger logDao = LoggerFactory.getLogger(EntryDaoJdbcImpl.class);
    private static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS entry (Id INT PRIMARY KEY AUTO_INCREMENT, field INT)";
    private static final String DROP_TABLE = "DROP TABLE IF EXISTS entry";
    private static final String TRUNCATE_TABLE = "TRUNCATE TABLE entry";
    private static final String SAVE_FIELD = "INSERT INTO entry (field) VALUES (?)";
    private static final String SELECT_FIELD = "SELECT field FROM entry";

    public boolean tableExists() {
        boolean check;
        try (Connection conn = ConnectionManager.open()) {
            DatabaseMetaData md = conn.getMetaData();
            ResultSet rs = md.getTables(null, null, "entry", null);
            check = rs.next();
        } catch (SQLException e) {
            logDao.error("Connection was fail" + e);
            throw new RuntimeException(e);
        }
        return check;
    }

    public void truncateTable() {
        try (Connection conn = ConnectionManager.open()) {
            try (PreparedStatement ps = conn.prepareStatement(TRUNCATE_TABLE)) {
                ps.execute();
                logDao.debug("Table was truncate");
            } catch (SQLException e) {
                conn.rollback();
                logDao.error("Table was not truncate, transaction rollback \n" + e);
                throw new RuntimeException(e);
            }
        } catch (SQLException e) {
            logDao.error("Connection was fail" + e);
            throw new RuntimeException(e);
        }
    }

    public void createTable() {

        try (Connection conn = ConnectionManager.open()) {
            try (PreparedStatement ps = conn.prepareStatement(CREATE_TABLE)) {
                ps.execute();
                logDao.debug("Table was created");
            } catch (SQLException e) {
                conn.rollback();
                logDao.error("Table was not created \n" + e);
                throw new RuntimeException(e);
            }
        } catch (SQLException e) {
            logDao.error("Connection was fail" + e);
            throw new RuntimeException(e);
        }
    }

    public void dropTable() {
        try (Connection conn = ConnectionManager.open()) {
            try (PreparedStatement ps = conn.prepareStatement(DROP_TABLE)) {
                ps.execute();
                logDao.debug("Table was dropped");
            } catch (SQLException e) {
                conn.rollback();
                logDao.error("Table was not dropped, transaction rollback \n" + e);
                throw new RuntimeException(e);
            }
        } catch (SQLException e) {
            logDao.error("Connection was fail\n" + e);
            throw new RuntimeException(e);
        }
    }

    public void saveField(int field) {
        try (Connection conn = ConnectionManager.open()) {
            try (PreparedStatement ps = conn.prepareStatement(SAVE_FIELD)) {
                ps.setInt(1, field);
                ps.executeUpdate();
                conn.commit();
                logDao.debug("Field with number " + field + " was added to database");
            } catch (SQLException e) {
                conn.rollback();
                logDao.error("Field was not added to database, transaction rollback\n " + e);
            }
        } catch (SQLException e) {
            logDao.error("Connection was fail\n" + e);
            e.printStackTrace();
        }
    }

    public List<Integer> getField() {
        List<Integer> fields = new ArrayList<>();
        try (Connection conn = ConnectionManager.open()) {
            try (PreparedStatement ps = conn.prepareStatement(SELECT_FIELD)) {
                ResultSet resultSet = ps.executeQuery();
                while (resultSet.next()) {
                    fields.add(resultSet.getObject("field", Integer.class)); // null safe
                }
                conn.commit();
                logDao.debug(" Field was select from database");
            } catch (SQLException e) {
                conn.rollback();
                logDao.error("Field was not select from database, transaction rollback\n " + e);
            }
        } catch (SQLException e) {
            logDao.error("Connection was fail\n" + e);
            e.printStackTrace();
        }
        return fields;
    }
}


