package org.example.dao;

import java.util.List;

public interface EntryDao {

    boolean tableExists();
    void truncateTable();
    void dropTable();
    void createTable();
    void saveField(int field);
    List<Integer> getField();
}
