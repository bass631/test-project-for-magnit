package org.example.service;

import org.example.dao.EntryDao;
import org.example.dao.EntryDaoJdbcImpl;

import java.util.List;

public class EntryServiceImpl implements EntryService {
    private final EntryDao entryDao = new EntryDaoJdbcImpl();
    public boolean tableExists(){
        return entryDao.tableExists();
    }
    public void truncateTable(){
        entryDao.truncateTable();
    }
    public void createTable() {
        entryDao.createTable();
    }
    public void dropTable() {
        entryDao.dropTable();
    }
    public void saveField(int field) {
        entryDao.saveField(field);
    }
    public List<Integer> getField() {
        return entryDao.getField();
    }
}
