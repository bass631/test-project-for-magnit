package org.example.service;

import java.util.List;

public interface EntryService {
    boolean tableExists();
    void truncateTable();
    void createTable();
    void dropTable();
    void saveField(int field);
    List<Integer> getField();
}
